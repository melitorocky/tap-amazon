"""Custom client handling, including amazonStream base class."""

import requests,json,pprint
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable,cast

from sp_api.api import Orders,Reports,Finances
from sp_api.base import Marketplaces

from datetime import datetime
from datetime import timedelta
from dateutil import parser
import pendulum
import time
import psycopg2

from singer_sdk.streams import Stream

import logging
LOGGER = logging.getLogger(__name__)

# marketplaceId = "APJ6JRA9NG5V4"


class amazonStream(Stream):
    """Stream class for amazon streams."""

    is_sorted = True

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Return a generator of row-type dictionary objects.

        The optional `context` argument is used to identify a specific slice of the
        stream if partitioning is required for the stream. Most implementations do not
        require partitioning and should ignore the `context` argument.
        """

        streamName = type(self).__name__

        # map the inputs to the function blocks
        if streamName == 'OrdersStream':
            return self.OrdersStream(context)
        # elif streamName == 'ReportsStream':
        #     return self.ReportsStream(["GET_FLAT_FILE_OPEN_LISTINGS_DATA"],1)
        else:
            print(streamName + ' not found..')
            quit()

        # raise NotImplementedError("The method is not yet implemented (TODO)")

    # def ReportsStream(self, reportTypes, days):
    #     # Reports: Get Reports
    #     res = Reports(
    #         marketplace=getattr(Marketplaces, self.config["countryCode"]),
    #         credentials=self.get_credentials()
    #     ).get_reports(
    #         MarketplaceIds=marketplaceIds,
    #         reportTypes=reportTypes,
    #         # reportTypes=["GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_GENERAL"],
    #         createdSince=(datetime.utcnow() - timedelta(days=days)).isoformat(),
    #         # createdUntil=(datetime.utcnow() - timedelta(days=1)).isoformat()
    #     )

    #     # document_id = res.payload

    #     # res = Reports(
    #     #     marketplace=getattr(Marketplaces, self.config["countryCode"]),
    #     #     credentials=credentials
    #     # ).get_report_document(
    #     #     document_id="amzn1.spdoc.1.3.91c98b49-aebf-4907-90c7-4ab2c544aefe.T399G6MYWAH5Z.107",
    #     #     decrypt=True,
    #     # )

    #     # resulto = res.payload['document']

    #     return res.payload

    def OrdersStream(self,context):
        marketplaceIds = self.config["marketplaceIds"]

        replication_key = self.get_starting_timestamp(context)
        pendulum_start_date = cast(datetime, pendulum.parse(self.config["start_date"]))

        if replication_key == pendulum_start_date:
            jobId = self.config["jobId"]
            bookmark = self.config["bookmark"]
            replication_key = self.getLastSuccessfulRunReplicationKey(jobId=jobId, bookmark=bookmark, start_date=replication_key)


        # DEBUG: always use the config date
        # replication_key = self.get_starting_timestamp(context)

        #trasformazioni su start_date: aggiungo un secondo, trasformo in stringa, tolgo ultime cifre, rendo in formato iso
        replication_key = replication_key + timedelta(seconds=1)
        replication_key = replication_key.to_datetime_string()
        replication_key = parser.parse(replication_key[:19])
        LastUpdatedAfter = replication_key.isoformat()

        # getOrders
        res = Orders(
            marketplace=getattr(Marketplaces, self.config["countryCode"]),
            credentials=self.get_credentials()
        ).get_orders(
            LastUpdatedAfter = LastUpdatedAfter,
            LastUpdatedBefore = (datetime.utcnow() - timedelta(hours=0.1)).isoformat(),
            MaxResultsPerPage = self.config["maxNumOrders"],
            MarketplaceIds=marketplaceIds
        )

        orders = res.payload['Orders']
        finalRes = []
        result = {}

        for order in orders:
            # getOrder
            res = Orders(
                marketplace=getattr(Marketplaces, self.config["countryCode"]),
                credentials=self.get_credentials()
            ).get_order(order_id = order["AmazonOrderId"])
            result = res.payload

            # getOrderBuyerInfo
            res = Orders(
                marketplace=getattr(Marketplaces, self.config["countryCode"]),
                credentials=self.get_credentials()
            ).get_order_buyer_info(order_id = order["AmazonOrderId"])
            result["OrderBuyerInfo"] = res.payload

            # getOrderAddress
            res = Orders(
                marketplace=getattr(Marketplaces, self.config["countryCode"]),
                credentials=self.get_credentials()
            ).get_order_address(order_id = order["AmazonOrderId"])
            result["OrderAddress"] = res.payload

            # getOrderItems
            res = Orders(
                marketplace=getattr(Marketplaces, self.config["countryCode"]),
                credentials=self.get_credentials()
            ).get_order_items(order_id = order["AmazonOrderId"])
            result["OrderItems"] = res.payload

            # getOrderItemsBuyerInfo
            res = Orders(
                marketplace=getattr(Marketplaces, self.config["countryCode"]),
                credentials=self.get_credentials()
            ).get_order_items_buyer_info(order_id = order["AmazonOrderId"])
            result["OrderItemsBuyerInfo"] = res.payload

            # financialEvents
            res = Finances(
                marketplace=getattr(Marketplaces, self.config["countryCode"]),
                credentials=self.get_credentials()
            ).get_financial_events_for_order(order_id = order["AmazonOrderId"])
            result["FinancialEvents"] = res.payload["FinancialEvents"]

            finalRes.append(result)

            # time.sleep(1)

        return finalRes


    def get_credentials(self):
        credentials = {
            'refresh_token': self.config["refresh_token"],
            'lwa_app_id': self.config["lwa_app_id"],
            'lwa_client_secret': self.config["lwa_client_secret"],
            'aws_access_key': self.config["aws_access_key"],
            'aws_secret_key': self.config["aws_secret_key"],
            'role_arn': self.config["role_arn"],
        }
        return credentials

    # def hour_rounder(self,t):
    #     return t.replace(second=0, microsecond=0, minute=0, hour=0)

    def getLastSuccessfulRunReplicationKey(self, jobId, bookmark, start_date):
        replication_key = start_date
        jsonPayload = '{}'

        conn = None
        try:
            # DB Connection
            conn = psycopg2.connect(
                host=self.config["postgres_remote_host"],
                database=self.config["postgres_remote_database"],
                user=self.config["postgres_remote_user"],
                password=self.config["postgres_remote_password"]
            )

            # Create a cursor
            cur = conn.cursor()

            # Define the query
            query = f"""SELECT payload
                FROM runs
                WHERE
                job_name = '{jobId}' AND
                state = 'SUCCESS'
                ORDER BY id DESC
                LIMIT 1"""

            # Execute the query
            cur.execute(query)

            # Fetch the first row
            queryResult = cur.fetchone()

            if queryResult:
                jsonPayload = queryResult[0]

            # Close the communication with the PostgreSQL
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            quit()
        finally:
            if conn is not None:
                conn.close()
                # print('Database connection closed.')

        # LOGGER.warning("------------------------------------------")
        # LOGGER.warning(type(jsonPayload))
        # LOGGER.warning(jsonPayload)
        # LOGGER.warning(start_date)
        # LOGGER.warning(replication_key)
        # LOGGER.warning("------------------------------------------")

        dictPayload = json.loads(jsonPayload)

        if self.keys_exists(dictPayload, "singer_state", "bookmarks", bookmark, "replication_key_value"):
            replication_key = dictPayload['singer_state']['bookmarks'][bookmark]['replication_key_value']
            replication_key = cast(datetime, pendulum.parse(replication_key))

        return replication_key

    def keys_exists(self, element, *keys):
        '''Check if *keys (nested) exists in `element` (dict).'''
        if not isinstance(element, dict):
            raise AttributeError('keys_exists() expects dict as first argument.')
        if len(keys) == 0:
            raise AttributeError('keys_exists() expects at least two arguments, one given.')

        _element = element
        for key in keys:
            try:
                _element = _element[key]
            except KeyError:
                return False
        return True
