"""Stream type classes for tap-amazon."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_amazon.client import amazonStream

# TODO: Delete this is if not using json files for schema definition
# SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class OrdersStream(amazonStream):
    """Define custom stream."""
    name = "orders"
    path = "/orders/v0/orders"
    primary_keys = ["AmazonOrderId"]
    replication_key = "LastUpdateDate"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "orders.json"
    schema = th.PropertiesList(
        # Order
        # th.Property("BuyerInfo", th.StringType),# NOTE: Undocumented (as of 22/10/2021) addition
        th.Property("AmazonOrderId", th.StringType),
        th.Property("PurchaseDate", th.DateTimeType),
        th.Property("LastUpdateDate", th.DateTimeType),
        th.Property("OrderStatus", th.StringType),
        th.Property("SellerOrderId", th.StringType),
        th.Property("FulfillmentChannel", th.StringType),
        th.Property("SalesChannel", th.StringType),
        th.Property("ShipServiceLevel", th.StringType),
        th.Property("OrderTotal", th.ObjectType(
            th.Property("CurrencyCode", th.StringType),
            th.Property("Amount", th.NumberType)
            )
        ),
        th.Property("NumberOfItemsShipped", th.IntegerType),
        th.Property("NumberOfItemsUnshipped", th.IntegerType),
        th.Property("PaymentMethod", th.StringType),
        th.Property("PaymentMethodDetails", th.ArrayType(
            th.StringType
        )),
        th.Property("IsReplacementOrder", th.BooleanType),
        th.Property("MarketplaceId", th.StringType),
        th.Property("ShipmentServiceLevelCategory", th.StringType),
        th.Property("OrderType", th.StringType),
        th.Property("EarliestShipDate", th.DateTimeType),
        th.Property("LatestShipDate", th.DateTimeType),
        th.Property("IsBusinessOrder", th.BooleanType),
        th.Property("IsSoldByAB", th.BooleanType),
        th.Property("IsPrime", th.BooleanType),
        th.Property("IsGlobalExpressEnabled", th.BooleanType),
        th.Property("IsPremiumOrder", th.BooleanType),
        th.Property("IsISPU", th.BooleanType),

        # OrderBuyerInfo
        th.Property("OrderBuyerInfo", th.ObjectType(
            th.Property("AmazonOrderId", th.StringType),
            th.Property("BuyerEmail", th.StringType),
            th.Property("BuyerName", th.StringType),
            th.Property("BuyerTaxInfo", th.ObjectType(
                th.Property("CompanyLegalName", th.StringType)
                )   
            ),
            th.Property("PurchaseOrderNumber", th.IntegerType)
            )
        ),
        
        # OrderAddress
        th.Property("OrderAddress", th.ObjectType(
            th.Property("AmazonOrderId", th.StringType),
            th.Property("ShippingAddress", th.ObjectType(
                th.Property("City", th.StringType),
                th.Property("CountryCode", th.StringType),
                th.Property("Name", th.StringType),
                th.Property("PostalCode", th.StringType),
                th.Property("StateOrRegion", th.StringType),
                th.Property("AddressLine1", th.StringType),
                    )
                )
            )
        ),
        
        # OrderItems
        th.Property("OrderItems", th.ObjectType(
            th.Property("AmazonOrderId", th.StringType),
            th.Property("NextToken", th.StringType),
            th.Property("OrderItems", th.ArrayType(
                th.ObjectType(
                    th.Property("ASIN", th.StringType),
                    th.Property("OrderItemId", th.StringType),
                    th.Property("SellerSKU", th.StringType),
                    th.Property("Title", th.StringType),
                    th.Property("QuantityOrdered", th.IntegerType),
                    th.Property("QuantityShipped", th.IntegerType),
                    th.Property("PointsGranted", th.ObjectType(
                        th.Property("PointsNumber", th.NumberType),
                        th.Property("PointsMonetaryValue", th.ObjectType(
                            th.Property("CurrencyCode", th.StringType),
                            th.Property("Amount", th.NumberType)
                                )
                            )
                        )
                    ),
                    th.Property("ItemPrice", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                    th.Property("ShippingPrice", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                    th.Property("ScheduledDeliveryEndDate", th.DateTimeType),
                    th.Property("ScheduledDeliveryStartDate", th.DateTimeType),
                    th.Property("CODFee", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                    th.Property("CODFeeDiscount", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                    th.Property("priceDesignation", th.NumberType),
                    th.Property("IsGift", th.BooleanType),
                    th.Property("IsTransparency", th.BooleanType),
                    
                    th.Property("ItemTax", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                    th.Property("OrderItemId", th.StringType),
                    th.Property("ProductInfo", th.ObjectType(
                        th.Property("NumberOfItems", th.NumberType)
                        )
                    ),
                    th.Property("IossNumber", th.StringType),
                    th.Property("OrderItemsBuyerInfo", th.ObjectType(
                        th.Property("GiftWrapPrice", th.ObjectType(
                            th.Property("CurrencyCode", th.StringType),
                            th.Property("Amount", th.NumberType)
                            )
                        ),
                        th.Property("GiftWrapTax", th.ObjectType(
                            th.Property("CurrencyCode", th.StringType),
                            th.Property("Amount", th.NumberType)
                            )
                        )
                    )
                    ),
                    th.Property("PromotionDiscount", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                    th.Property("PromotionDiscountTax", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                    th.Property("ShippingDiscount", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                    th.Property("ShippingDiscountTax", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                    th.Property("ShippingTax", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                )
            ))
        )),

        # OrderItemsBuyerInfo
        th.Property("OrderItemsBuyerInfo", th.ObjectType(
            th.Property("AmazonOrderId", th.StringType),
            th.Property("OrderItems", th.ArrayType(
                th.ObjectType(
                    th.Property("BuyerCustomizedInfo", th.ObjectType(
                        th.Property("CustomizedURL", th.StringType)
                        )
                    ),
                    th.Property("GiftMessageText", th.StringType),
                    th.Property("GiftWrapPrice", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                    th.Property("GiftWrapTax", th.ObjectType(
                        th.Property("CurrencyCode", th.StringType),
                        th.Property("Amount", th.NumberType)
                        )
                    ),
                    th.Property("OrderItemId", th.StringType),
                    th.Property("GiftWrapLevel", th.StringType)
                )
            ))
        )),
        
        # FinancialEvents
        th.Property("FinancialEvents", th.ObjectType(
            # RefundEventList
            th.Property("RefundEventList", th.ArrayType(
                th.ObjectType(
                    th.Property("AmazonOrderId", th.StringType),
                    th.Property("SellerOrderId", th.StringType),
                    th.Property("MarketplaceName", th.StringType),
                    # th.Property("PostedDate", th.DateTimeType),
                    th.Property("PostedDate", th.StringType),
                    th.Property("ShipmentItemAdjustmentList", th.ArrayType(
                        th.ObjectType(
                            th.Property("SellerSKU", th.StringType),
                            th.Property("OrderAdjustmentItemId", th.StringType),
                            th.Property("QuantityShipped", th.NumberType),
                            th.Property("ItemChargeAdjustmentList", th.ArrayType(
                                th.ObjectType(
                                    th.Property("ChargeType", th.StringType),
                                    th.Property("ChargeAmount", th.ObjectType(
                                        th.Property("CurrencyCode", th.StringType),
                                        th.Property("CurrencyAmount", th.NumberType),
                                    )),
                                )
                            )),
                            th.Property("ItemFeeAdjustmentList", th.ArrayType(
                                th.ObjectType(
                                    th.Property("FeeType", th.StringType),
                                    th.Property("FeeAmount", th.ObjectType(
                                        th.Property("CurrencyCode", th.StringType),
                                        th.Property("CurrencyAmount", th.NumberType),
                                    )),
                                )
                            )),
                        )
                    )),
                )
            )),
        )),
    
    ).to_dict()

    


# class ReportsStream(amazonStream):
#     """Define custom stream."""
#     name = "reports"
#     path = "/reports"
#     primary_keys = ["reportId"]
#     replication_key = "processingStatus"
#     schema = th.PropertiesList(
#         th.Property("reportType", th.StringType),
#         th.Property("processingEndTime", th.DateTimeType),
#         th.Property("processingStatus", th.StringType),
#         th.Property("marketplaceIds", th.ArrayType(
#             th.StringType
#             )
#         ),
#         th.Property("reportDocumentId", th.StringType),
#         th.Property("reportId", th.StringType),
#         th.Property("dataEndTime", th.DateTimeType),
#         th.Property("createdTime", th.DateTimeType),
#         th.Property("processingStartTime", th.DateTimeType),
#         th.Property("dataStartTime", th.DateTimeType)
#     ).to_dict()